import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class ShopView extends  JPanel{

        private JPanel rightPanel, leftPanel;
        private JLabel topLabel;
        private JFrame frame;
        private JButton product1, product2, product3;
        private JButton vegetables, orange, watermelon, lemon, Vodka, Whiskey, Beer;


        public  static void main(String[] args) {
            new ShopView();
        }

        public ShopView()
        {

            frame = new JFrame("Domashno");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(900,900);
            frame.setVisible(true);
            frame.setLayout(null);

            topLabel = new JLabel("Misho's Market", JLabel.LEFT);
            Border blackLine = BorderFactory.createLineBorder(Color.BLACK);
            topLabel.setBounds(0,0,900,50);
            topLabel.setBorder(blackLine);
            frame.add(topLabel);

            rightPanel = new JPanel();
            rightPanel.setBounds( 300,50,600,900);
            rightPanel.setBorder(blackLine);
            frame.add(rightPanel);

            leftPanel = new JPanel();
            leftPanel.setBounds(0,50,500,900);
            leftPanel.setBorder(blackLine);
            frame.add(leftPanel);

            product1 = new JButton("Vegetables");
            product1.setBounds(4,60,120,25);
            frame.add(product1);

            vegetables = new JButton("Green Vegetables");
            vegetables.setBounds(50,120,140,25);
            frame.add(vegetables);

            product2 = new JButton("Fruits");
            product2.setBounds(4,180,100,25);
            frame.add(product2);

            orange = new JButton("Orange");
            orange.setBounds(50,240,140,25);
            frame.add(orange);

            watermelon = new JButton("Watermelon");
            watermelon.setBounds(50,300,140,25);
            frame.add(watermelon);

            lemon = new JButton("Lemon");
            lemon.setBounds(50,360,140,25);
            frame.add(lemon);

            product3 = new JButton("Alcohol");
            product3.setBounds(4,420,100,25);
            frame.add(product3);

            Vodka = new JButton("Vodka");
            Vodka.setBounds(50,480,140,25);
            frame.add(Vodka);

            Whiskey = new JButton("Whiskey");
            Whiskey.setBounds(50,540,140,25);
            frame.add(Whiskey);

            Beer = new JButton("Beer");
            Beer.setBounds(50,600,140,25);
            frame.add(Beer);
        }
}
