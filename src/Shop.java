import java.util.List;

public class Shop {

    private String name;
    private List<Category> category;
    private List<Payment> payTypes;
    private Cart shoppingCart;

    public Shop(String name, List<Category> categories, List<Payment> payTypes, Cart shoppingCart) {
        this.name = name;
        this.category = categories;
        this.payTypes = payTypes;
        this.shoppingCart = shoppingCart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public List<Payment> getPayTypes() {
        return payTypes;
    }

    public void setPayTypes(List<Payment> payTypes) {
        this.payTypes = payTypes;
    }

    public Cart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(Cart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }
}
