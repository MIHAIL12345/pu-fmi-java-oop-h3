public class SelectedProduct extends Product{

    private int productCount;
    private int productTotalPrice;

    public SelectedProduct(String brandName, double price, int quantity,
                           int productCount, int productTotalPrice) {
        super(brandName, price, quantity);
        this.productCount = productCount;
        this.productTotalPrice = productTotalPrice;
    }

    public int getProductCount() {
        return productCount;
    }

    public int getProductTotalPrice() {
        return productTotalPrice;
    }

    public void setProductCount(int productCount) {
        this.productCount = productCount;
    }

    public void setProductTotalPrice(int productTotalPrice) {
        this.productTotalPrice = productTotalPrice;
    }
    public double getTotalPrice(){
        return  getProductCount()*getPrice();
    }


}
