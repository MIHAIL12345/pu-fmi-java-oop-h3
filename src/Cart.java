import java.util.List;

public class Cart implements Manage<SelectedProduct> {
    List<SelectedProduct> selectedProducts;

    public Cart(List<SelectedProduct> selectedProduct) {
        this.selectedProducts = selectedProduct;
    }

    public void add(SelectedProduct newSelectedProduct) {
        selectedProducts.add(newSelectedProduct);
    }

    public void remove(SelectedProduct product) {
        selectedProducts.remove(product);
    }

    public List<SelectedProduct> getSelectedProducts() {
        return selectedProducts;
    }

    public void setSelectedProducts(List<SelectedProduct> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }

    @Override
    public void add() {

    }

    @Override
    public void remove() {

    }
}
